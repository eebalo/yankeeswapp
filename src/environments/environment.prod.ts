export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCB9fN1bYQsbimSXHpcMV0_y7iDDt-tWAc",
    authDomain: "whiteelephant-eebalo.firebaseapp.com",
    databaseURL: "https://whiteelephant-eebalo.firebaseio.com",
    projectId: "whiteelephant-eebalo",
    storageBucket: "whiteelephant-eebalo.appspot.com",
    messagingSenderId: "955455766726",
    appId: "1:955455766726:web:301b98549f9631ff0a97c7",
    measurementId: "G-CTEMSV4ZLD"
  }
};
