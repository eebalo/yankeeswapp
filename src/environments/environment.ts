// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCB9fN1bYQsbimSXHpcMV0_y7iDDt-tWAc",
    authDomain: "whiteelephant-eebalo.firebaseapp.com",
    databaseURL: "https://whiteelephant-eebalo.firebaseio.com",
    projectId: "whiteelephant-eebalo",
    storageBucket: "whiteelephant-eebalo.appspot.com",
    messagingSenderId: "955455766726",
    appId: "1:955455766726:web:301b98549f9631ff0a97c7",
    measurementId: "G-CTEMSV4ZLD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
