import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { GameService } from '../game.service';
import { Gift } from '../game.model';
import { Game } from '../game.model';
import { MatDialog } from '@angular/material/dialog';
import { GiftDialogComponent } from '../dialogs/gift-dialog.component';
import { StealGiftDialogComponent } from '../dialogs/steal-gift-dialog.component';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs';
import { BreakpointObserverService } from '../../services/breakpoint-observer.service'

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent {
  @Input() game;
  gameId: string;
  sub: Subscription;
  theGame: Game;
  cols: number;

  public size$: Observable<string>;

  constructor(private gameService: GameService, 
              private dialog: MatDialog, 
              private route: ActivatedRoute,
              private _breakpointObserverService: BreakpointObserverService) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.gameId = params.get("gameId");
      this.sub = this.gameService
        .getSingleGame(this.gameId)
        .subscribe(game => {
          this.theGame = game;
          this.theGame.id = this.gameId;
        });
    });
    this.cols = 4;
    this.size$ = this._breakpointObserverService.size$;
  }

  giftDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.game.gifts, event.previousIndex, event.currentIndex);
    this.gameService.updateGifts(this.game.id, this.game.gifts);
  }

  openDialog(gift?: Gift, idx?: number): void {
    const newGift = { steals: 0 };
    const dialogRef = this.dialog.open(GiftDialogComponent, {
      width: '400px',
      data: gift
        ? { gift: { ...gift }, isNew: false, gameId: this.theGame.id, idx }
        : { gift: newGift, isNew: true }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        if (result.isNew) {
          this.gameService.updateGifts(this.theGame.id, [
            ...this.theGame.gifts,
            result.gift
          ]);
        } else {
          const update = this.theGame.gifts;
          update.splice(result.idx, 1, result.gift);
          this.gameService.updateGifts(this.theGame.id, this.theGame.gifts);
        }
      }
    });
  }

  stealGift(gift?: Gift, idx?: number): void {
    // const update = this.theGame.gifts;
    // gift.steals = gift.steals + 1;
    // update.splice(idx, 1, gift);
    // this.gameService.updateGifts(this.theGame.id, this.theGame.gifts);
    // 
    const newGift = { steals: 0 };
    console.log(gift);
    const dialogRef = this.dialog.open(StealGiftDialogComponent, {
      width: '400px',
      data: gift
        ? { gift: { ...gift }, isNew: false, gameId: this.theGame.id, idx }
        : { gift: newGift, isNew: true }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        const update = this.theGame.gifts;
        result.gift.steals = result.gift.steals + 1;  //increase count
        update.splice(result.idx, 1, result.gift);
        this.gameService.updateGifts(this.theGame.id, this.theGame.gifts);
      }
    });
  }

  handleGiftDelete(gift?: Gift) {
    // this.gameService.deleteGame(this.theGame.id);
    this.gameService.removeGift(this.theGame.id, gift)
  }
}
