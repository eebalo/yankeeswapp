import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { switchMap, map } from 'rxjs/operators';
import { Game, Gift } from './game.model';
import { pipe } from 'rxjs';
import { GameComponent } from './game/game.component';
import { GameModule } from './game.module';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  constructor(private afAuth: AngularFireAuth, private db: AngularFirestore) {}

  /**
   * Creates a new game for the current user
   */
  async createGame(data: Game) {
    const user = await this.afAuth.auth.currentUser;
    return this.db.collection('games').add({
      ...data,
      uid: user.uid,
      gifts: [{ description: 'The first gift!', steals: 0 }]
    });
  }

  /**
   * Delete game
   */
  deleteGame(gameId: string) {
    return this.db
      .collection('games')
      .doc(gameId)
      .delete();
  }

  /**
   * Updates the gifts on game
   */
  updateGifts(gameId: string, gifts: Gift[]) {
    return this.db
      .collection('games')
      .doc(gameId)
      .update({ gifts });
  }

  /**
   * Remove a specifc gift from the game
   */
  removeGift(gameId: string, gift: Gift) {
    return this.db
      .collection('games')
      .doc(gameId)
      .update({
        gifts: firebase.firestore.FieldValue.arrayRemove(gift)
      });
  }

  /**
   * Get all games owned by current user
   */
  getUserGames() {
    return this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          // return this.db
          //   .collection<Game>('games', ref =>
          //     ref.where('uid', '==', user.uid)
          //   )
          //   .valueChanges({ idField: 'id' });
          
          return this.db
            .collection<Game>('games')
            .valueChanges({ idField: 'id' });
        } else {
          return [];
        }
      })
    );
  }

  /**
   * Get specific game by current user
   * @param games 
   */
  getSingleGame(gameId: string) {
    // return this.db
    //   .collection('games')
    //   .doc(gameId).ref.get()
    //   .then(function (doc) {
    //     if (doc.exists) {
    //       console.log(doc.data());
    //       return doc;
    //     } else {
    //       console.log("There is no document!");
    //       return {gifts: Array(1), priority: 0, title: "Control Systems 2019", uid: "ZNHBtESsEgcW4xoHRnqWtRAPAUr1"};
    //     }
    // }).catch(function (error) {
    //   console.log("There was an error getting your document:", error);
    //   return {gifts: Array(1), priority: 0, title: "Control Systems 2019", uid: "ZNHBtESsEgcW4xoHRnqWtRAPAUr1"};
    // });
    // console.log('gameId');console.log(gameId);
    return this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.db
            .doc<Game>('games/' + gameId)
            .valueChanges();
            // .collection<Game>('games/', ref =>
            //   ref.where('id', '==', gameId)
            // )
            // .valueChanges({ idField: 'id' });
        } else {
          var tmpGame: Game;
          tmpGame = {id: 'BM93fRV9wsJyV7FL3GIw', title: "Control Systems 2019" }
          // tmpGame = {id: 'BM93fRV9wsJyV7FL3GIw', title: "Control Systems 2019", uid: "ZNHBtESsEgcW4xoHRnqWtRAPAUr1", giftaas:[] }
          return [tmpGame];
        }
      })
    );
  }

  /**
   * Run a batch write to change the priority of each game for sorting
   */
  sortGames(games: Game[]) {
    const db = firebase.firestore();
    const batch = db.batch();
    const refs = games.map(b => db.collection('games').doc(b.id));
    refs.forEach((ref, idx) => batch.update(ref, { priority: idx }));
    batch.commit();
  }
}
