import { Component, OnInit, OnDestroy } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Subscription } from 'rxjs';
import { Game } from '../game.model';
import { GameService } from '../game.service';
import { MatDialog } from '@angular/material/dialog';
import { GameDialogComponent } from '../dialogs/game-dialog.component';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit, OnDestroy {
  games: Game[];
  sub: Subscription;

  constructor(public gameService: GameService, public dialog: MatDialog) {}

  ngOnInit() {
    this.sub = this.gameService
      .getUserGames()
      .subscribe(games => (this.games = games));
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.games, event.previousIndex, event.currentIndex);
    this.gameService.sortGames(this.games);
  }

  openGameDialog(): void {
    const dialogRef = this.dialog.open(GameDialogComponent, {
      width: '400px',
      data: {  }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.gameService.createGame({
          title: result
        });
      }
    });
  }
}

