export interface Game {
    id?: string;
    title?: string;
    gifts?: Gift[];
  }

export interface Gift {
  description?: string;
  participant?: string;
  steals?: number;
}