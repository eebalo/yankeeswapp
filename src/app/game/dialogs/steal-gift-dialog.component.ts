import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GameService } from '../game.service';

@Component({
  selector: 'app-steal-gift-dialog',
  template: `
    <h1 mat-dialog-title>Gift</h1>
    <div mat-dialog-content class="content">
      <mat-form-field>
        <input 
          placeholder="Participant" 
          matInput
          [(ngModel)]="data.gift.participant" />
      </mat-form-field>
    </div>
    <div mat-dialog-actions>
      <button mat-raised-button color="primary" [mat-dialog-close]="data" cdkFocusInitial>
        Steal
      </button>
      &nbsp;
    </div>
  `,
  styles: []
})
export class StealGiftDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<StealGiftDialogComponent>,
    private ps: GameService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  handleGiftDelete() {
    this.ps.removeGift(this.data.gameId, this.data.gift);
    this.dialogRef.close();
  }

}
