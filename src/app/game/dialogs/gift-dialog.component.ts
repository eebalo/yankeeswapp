import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GameService } from '../game.service';

@Component({
  selector: 'app-gift-dialog',
  template: `
    <h1 mat-dialog-title>Gift</h1>
    <div mat-dialog-content class="content">
      <mat-form-field>
        <input 
          placeholder="Gift description" 
          matInput
          [(ngModel)]="data.gift.description" />
      </mat-form-field>
      <br />
      <mat-form-field>
        <input 
          placeholder="Participant" 
          matInput
          [(ngModel)]="data.gift.participant" />
      </mat-form-field>
      <br />
      <mat-form-field *ngIf="!data.isNew">
        <input type="number" min="0" max="3"
          placeholder="Number of times stolen" 
          matInput
          [(ngModel)]="data.gift.steals" />
      </mat-form-field>
    </div>
    <div mat-dialog-actions>
      <button mat-raised-button color="primary" [mat-dialog-close]="data" cdkFocusInitial>
        {{ data.isNew ? 'Add Gift' : 'Update Gift' }}
      </button>
      &nbsp;
      <app-delete-button (delete)="handleGiftDelete(gift)" *ngIf="!data.isNew"></app-delete-button>
    </div>
  `,
  styles: []
})
export class GiftDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<GiftDialogComponent>,
    private ps: GameService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  handleGiftDelete() {
    this.ps.removeGift(this.data.gameId, this.data.gift);
    this.dialogRef.close();
  }

}
