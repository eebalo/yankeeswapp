import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-game-dialog',
  template: `
  <h1 mat-dialog-title>Game</h1>
  <div mat-dialog-content>
  <p>What shall we call this game?</p>
    <mat-form-field>
      <input placeholder="Title" matInput [(ngModel)]="data.title" />
    </mat-form-field>
  </div>
  <div mat-dialog-actions>
    <button mat-button (click)="onNoClick()">Cancel</button>
    <button mat-button [mat-dialog-close]="data.title" cdkFocusInitial>
      Create
    </button>
  `,
  styles: []
})
export class GameDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<GameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }


}
