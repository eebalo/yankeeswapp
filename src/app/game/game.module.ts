import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameRoutingModule } from './game-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { GameListComponent } from './game-list/game-list.component';
import { GameComponent } from './game/game.component';
import { GameDialogComponent } from './dialogs/game-dialog.component';
import { GiftDialogComponent } from './dialogs/gift-dialog.component';
import { StealGiftDialogComponent } from './dialogs/steal-gift-dialog.component';


@NgModule({
  declarations: [GameListComponent, GameComponent, GameDialogComponent, GiftDialogComponent, StealGiftDialogComponent],
  imports: [
    CommonModule,
    GameRoutingModule,
    SharedModule,
    FormsModule,
    DragDropModule,
    MatDialogModule,
    MatButtonToggleModule,
  ],
  entryComponents: [GameDialogComponent, GiftDialogComponent, StealGiftDialogComponent]
})
export class GameModule { }
