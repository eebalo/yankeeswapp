import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { AuthGuard } from './user/auth.guard';
import { NotfoundComponent } from "./shared/notfound/notfound.component";

const routes: Routes = [
  { 
    path: '', 
    component: HomePageComponent,
  },
  {
    path: 'login', 
    loadChildren: () =>
      import ('./user/user.module').then(m => m.UserModule)
  },
  {
    path: 'game', 
    loadChildren: () => 
      import ('./game/game.module').then(m => m.GameModule),
    // canActivate: [AuthGuard]
  },
  { path: '**', component: NotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
